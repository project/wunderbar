(function ($) {
$(document).ready(function(){
  $("#branding-bar .bar-search-btn").click(function(){
    $("#branding-bar .bar-search form").submit();
  });

  $(".bar-btn").toggle(function(){
    if( $.easing.def ) {
      $("#branding-bar-content").animate({width: totalwidth(".bar-user")}, 500, "easeOutQuart");
    }
    else {
        $("#branding-bar-content").animate({width: totalwidth(".bar-user")}, 500);
    }
      if( $.cookie ) {
      $.cookie('wunderbar', 'closed');
    }
    $('.bar-btn').removeClass('close').addClass('open');
  },function(){
    if( $.cookie ) {
      $.cookie('wunderbar', 'open');
    }
    var bar_w = 0;
    $("#branding-bar-content div:not(.bar-search div)").each(function(){
    bar_w += totalwidth(this);
    });
    if( $.easing.def ) {
      $("#branding-bar-content").animate({width: bar_w-18}, 500, "easeOutQuart");
    }
    else {
      $("#branding-bar-content").animate({width: bar_w-18}, 500);
    }
    $('.bar-btn').removeClass('open').addClass('close');
  });
  if( $.cookie ) {
    if ($.cookie('wunderbar') == 'closed') {
      $("#branding-bar-content").css('width',totalwidth(".bar-user"));
    $(".bar-btn").click();
    }
  }
  else {
  $("#branding-bar-content").css('width',totalwidth(".bar-user"));
    $(".bar-btn").click();  
  }
});

function totalwidth(obj) {
  return $(obj).width() + parseInt($(obj).css('margin-right')) + parseInt($(obj).css('margin-left')) + parseInt($(obj).css('padding-left')) + parseInt($(obj).css('padding-left'));
}
})(jQuery);