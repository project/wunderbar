ELMS: Wunderbar - Branding, account management and social networking linkage all in a cool little bar!
Copyright (C) 2009-2010  The Pennsylvania State University

Michael Collins
msc227@psu.edu

Bryan Ollendyke
bto108@psu.edu

Keith D. Bailey
kdb163@psu.edu

12 Borland
University Park, PA 16802

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Install:
1. Turn it on and go to the configuration page
2. Go to admin/user/permissions and give the roles you want the 'view wunderbar object' permission.
3. You're good to go!

Optional install:
1. Wunderbar supports the cookie plugin to remember the state of the bar when you open and close it.  To download cookie go to http://plugins.jquery.com/files/jquery.cookie.js.txt and put it in the wunderbar/js folder.
2. Wunderbar supports the easing plugin to make open / close of the toolbar nicer.  To download easing go to http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js and put it in the wunderbar/js folder.

Known to work in:
IE 8!!
Firefox 3 (Mac / PC)
Safari 4 (Mac / PC)
Seamonkey
Webkit
Flock
Opera 9+
Chrome

Doesn't work in:
Netscape Navigator 9 (CSS screwed up)
Camino (CSS screwed up)
IE 7 (CSS screwed up)